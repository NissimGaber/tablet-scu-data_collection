package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 5/15/2016.
 */
// Hijack these for simplicity

import android.util.Log;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.List;

public class WSPsmDataCyclicBuffer{

        private List<WSCyclicBufferEntity> buffer = new ArrayList<WSCyclicBufferEntity>();
        private int tail;
        private int head;
        private int numOfConsumers=0;



        public WSPsmDataCyclicBuffer(int n) {
            Log.d("WSPsmDataCyclicBuffer","Constructor create buffer size = " + n);
            for(int i =0;i<n;i++) {
                WSCyclicBufferEntity psmData = new WSCyclicBufferEntity();
                buffer.add(psmData);
            }
            numOfConsumers = 0;
            tail = 0;
            head = 0;
        }

        public void AddNewConsumer()
        {
            numOfConsumers++;
        }

        public void SetMatrixSize(int numOfColumns,int numOfRows)
        {
            Log.d("WSPsmDataCyclicBuffer","SetDataLength , buffer.length = " +buffer.size() + " , numOfColumns = "+ numOfColumns);
            for(WSCyclicBufferEntity rowDataObject : buffer){
                rowDataObject.SetMatrixSize(numOfColumns, numOfRows);
                //rowDataObject.numOfConsumersRead = listeners.size();
            }
        }

        public int add(int rowNumber,WSCyclicBufferEntity toAdd) {
            int index=0;
            if (head != (tail - 1)) {
                index = head;
                WSCyclicBufferEntity dataObject = buffer.get(index);
                for(int i=0;i<toAdd.psmData.mCapacitanceVec[rowNumber].length;i++) {
                    dataObject.psmData.mPressureVec[rowNumber][i] = toAdd.psmData.mPressureVec[rowNumber][i];
                    dataObject.psmData.mCapacitanceVec[rowNumber][i] = toAdd.psmData.mCapacitanceVec[rowNumber][i];
                }
                if(dataObject.psmData.mPressureVec.length == (rowNumber+1)) {
                    dataObject.numOfConsumersRead = 0;
                    head++;
                    head = head % buffer.size();
                    Log.d("WSPsmDataCyclicBuffer","Change Thread "+ Thread.currentThread().getName() + "  , Head = " + head);
                }
            } else {
                throw new BufferOverflowException();
            }
            return index;
        }

        public WSCyclicBufferEntity getByIndex(int index)
        {
            Log.d("WSPsmDataCyclicBuffer", "read data from index = " + index);
            if(index < buffer.size()) {
                return buffer.get(index);
            } else {
                Log.e("WSPsmDataCyclicBuffer", "index is bigger than the buffer length, size = "+ buffer.size()+" , index = " + index);
                throw new BufferUnderflowException();
            }
        }

        public boolean FinishDataConsuming(int index)
        {
            Log.d("WSPsmDataCyclicBuffer", "read data from index = " + index);
            WSCyclicBufferEntity psmData = null;
            if(index < buffer.size()) {
                psmData = buffer.get(index);
                psmData.numOfConsumersRead++;
                if((index == tail) && (psmData.numOfConsumersRead == numOfConsumers)) {
                    int adjTail = tail > head ? tail - buffer.size() : tail;
                    if (adjTail < head) {
                        tail++;
                        tail = tail % buffer.size();
                        Log.d("WSPsmDataCyclicBuffer","Change Thread "+ Thread.currentThread().getName() + " , Change Tail = " + tail + "Cell released");
                        return true;
                    } else {
                        throw new BufferUnderflowException();
                    }
                }
            } else {
                Log.e("WSPsmDataCyclicBuffer", "index is bigger than the buffer length, size = "+ buffer.size()+" , index = " + index);
                throw new BufferUnderflowException();
            }
            return false;
        }


        public String toString() {
            return "CyclicBuffer(size=" + buffer.size() + ", head=" + head + ", tail=" + tail + ")";
        }

    }
