package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 5/18/2016.
 */
public interface WSIPsmDataConsumer {

    public void SetDataRead(boolean hasBeenRead);

    public boolean HasReadLastData();

    public void NewMessageArrived(WSDataCollectionMessage  dataCollectionMessag);

    public void AddDataReceiver(WSDataReceivedHub dataReceiver);

    public enum WSEDataCollectionMsg {
        kMessageUnkown(-1),
        kNewDataReceived(0);
        int id;

        WSEDataCollectionMsg(int i) {
            id = i;
        }
    }

    public class WSDataCollectionMessage {
        public WSEDataCollectionMsg eMsg = WSEDataCollectionMsg.kMessageUnkown;
        public int parameter;
    }
}