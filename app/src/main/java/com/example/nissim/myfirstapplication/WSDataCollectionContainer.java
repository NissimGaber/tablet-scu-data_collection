package com.example.nissim.myfirstapplication;

import android.content.Context;

/**
 * Description: This class creates the objects for the data collection and register the connections between the coponents
 * Usage: In order to activate the data collection sequence do the following:
 *        1- Create a data collection container
 *        2- Add =data consumers using the AddDataConsumer method
 *        3- run the OpenCommunicationToHwc
 *        4- run the StartDataCollectionSequence
 *        5- The data shall be received to the threads of the consumers
 *
 * Created by nissim on 5/10/2016.
 */
public class WSDataCollectionContainer {
    private WSPsmProxyImpl                                          psmProxy;
    private WSCalculationEngine                                     calculationEngine;
    private WSDataReceivedHub                                       dataReceiverHub;

    public WSDataCollectionContainer(Context context) {
        psmProxy = new WSPsmProxyImpl(context);
        calculationEngine = new WSCalculationEngine();
        dataReceiverHub = new WSDataReceivedHub();
    }

    public void OpenCommunicationToHwc()
    {
        psmProxy.RegisterRowDataConsumer(calculationEngine);
        psmProxy.RegisterRowDataConsumer(dataReceiverHub);
        calculationEngine.RegisterToCalculatedDataReady(dataReceiverHub);
        psmProxy.StartOperation();
    }
    public void StartDataCollectionSequence()
    {
        psmProxy.Ready();
    }

    public void AddDataConsumer(WSPsmDataConsumer dataConsumer)
    {
        dataReceiverHub.AddListener(dataConsumer);
    }
}
