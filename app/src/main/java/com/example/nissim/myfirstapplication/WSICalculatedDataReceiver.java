package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 5/9/2016.
 */
public interface WSICalculatedDataReceiver {
    public void PressureDataReceived(byte calculatedDataRow[], int rowNumber, byte len);
}
