package com.example.nissim.myfirstapplication;

import android.util.Log;

import com.example.nissim.myfirstapplication.WSCommPkgDefs.SubType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


/**
 * Created by nissim on 2/24/2016.
 */
public class WSCommPkgUtils {
    /////////////////////////////////////////////////////////////////////////////
//			Write
/////////////////////////////////////////////////////////////////////////////
    public static int WriteMsgFlag(byte[] buff){
        int rc = WriteInt32(WSCommPkgDefs.kMsgFlag, buff, WSCommPkgDefs.kPosMsgFlag);
        Log.d("WriteMsgFlag","data = " + buff);
        return rc;
    }

    public static int WriteMsgFrameNumber(byte fNum,byte[] buff){
        int rc = WriteInt8(fNum, buff, WSCommPkgDefs.kPosMsgFrameNum);
        Log.d("WriteMsgFrameNumber","data = " + buff);
        return rc;
    }

    public static int WriteMsgCategory(WSCommPkgDefs.Category cat, byte[] buff){
        int rc = WriteInt8(cat.getId(), buff,WSCommPkgDefs.kPosMsgCategory);
        Log.d("WriteMsgCategory","data = " + buff);
        return rc;
    }
    public static int WriteMsgSubType(SubType sub, byte[] buff){
        int rc = WriteInt8(sub.getId(), buff, WSCommPkgDefs.kPosMsgSubType);
        Log.d("WriteMsgSubType","data = " + buff[WSCommPkgDefs.kPosMsgSubType]);
        return rc;
    }
    public static int WriteMsgStatus(WSCommPkgDefs.Status stat, byte[] buff){
        int rc = WriteInt8(stat.getId(), buff, WSCommPkgDefs.kPosMsgStatus);
        Log.d("WriteMsgStatus","data = " + buff[WSCommPkgDefs.kPosMsgStatus]);
        return rc;
    }
    public static int WriteMsgLength(short len, byte[] buff){
        int rc = WriteInt16(len, buff, WSCommPkgDefs.kPosMsgLength);
        Log.d("WriteMsgLength","data = " + buff);
        return rc;
    }


    public static int WriteGeneralHeader(byte frameNum,
                                         WSCommPkgDefs.Category cat,
                                         SubType subType,
                                          byte[] buff){

        int msgLen = 0;
        Log.d("WriteGeneralHeader","Category = "+ cat.toString());
        msgLen += WriteMsgFlag(buff);
        msgLen += WriteMsgFrameNumber(frameNum,buff);
        msgLen += WriteMsgCategory(cat, buff);
        msgLen += WriteMsgSubType(subType, buff);
        return msgLen;
    }

    public static int WriteInt8(byte from, byte[] to, int pos){
        to[pos] = from;
        return 1;
    }

    public static int WriteInt16(short from, byte[] to, int pos){
        byte[] byteFrom  = new byte[2];
        byteFrom = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(from).array();
        System.arraycopy(byteFrom, 0, to, pos, 2);
        return 2;
    }
    public static int WriteInt32(int from, byte[] to, int pos){
        byte[] byteFrom  = new byte[4];
        byteFrom = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(from).array();
        System.arraycopy(byteFrom, 0, to, pos, 4);
        return 4;
    }

    public static int WriteFloat(float from, byte[] to, int pos){
        byte[] byteFrom  = new byte[4];
        byteFrom = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(from).array();
        System.arraycopy(byteFrom, 0, to, pos, 4);
        return 4;
    }

    public static int WriteArray(byte[] from, byte[] to, int len, int pos){
        System.arraycopy(from, 0, to, pos, len);
        return len;
    }


/////////////////////////////////////////////////////////////////////////////
//			Read
/////////////////////////////////////////////////////////////////////////////

// TODO - CR - make sure the buff includes all of the header fields so that the reading is from the
// fixed position, same as in the corresponding WriteXXX() functions

    public static WSCommPkgDefs.Category ReadMsgCategory(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgCategory) {
            byte temp = ReadInt8(buff, WSCommPkgDefs.kPosMsgCategory);
            //Log.d("ReadMsgCategory","Category number = " + temp);
            return (WSCommPkgDefs.Category.values()[temp]);
        }
        else {
            return (WSCommPkgDefs.Category.kEventNoEvent);
        }
    }
    public static SubType ReadMsgSubType(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgSubType){
            byte temp = ReadInt8(buff, WSCommPkgDefs.kPosMsgSubType);
            return (SubType.values()[temp]);
        }
        else {
            return (SubType.kEmptySubType);
        }
    }
    public static WSCommPkgDefs.Status ReadMsgStatus(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgStatus) {
            byte temp = ReadInt8(buff, WSCommPkgDefs.kPosMsgStatus);
            return (WSCommPkgDefs.Status.values()[temp]);
        }
        else {
            return (WSCommPkgDefs.Status.kStatusNoStatus);
        }
    }
    public static byte ReadMsgFrameNumber(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgFrameNum) {
            byte temp = ReadInt8(buff, WSCommPkgDefs.kPosMsgFrameNum);
            return temp;
        }
        else {
            return (-1);
        }
    }
    public static int ReadMsgFlag(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgFlag){
            int temp = ReadInt32(buff, WSCommPkgDefs.kPosMsgFlag);
            return temp;
        }
        else {
            return (-1);
        }
    }
    public static short ReadMsgLen(byte[] buff){
        if(buff.length > WSCommPkgDefs.kPosMsgLength) {
            short temp = ReadInt16(buff, WSCommPkgDefs.kPosMsgLength);
            return temp;
        }
        else {
            return (-1);
        }
    }

    public static byte ReadInt8(byte[] from,int pos){
        byte to = 0;
        if(from.length > pos) {
            to = from[pos];
        }
        else {
            Log.d("ReadInt8","From array length is smaller than pos, pos = " + pos + " , length = " + from.length);
        }
        return(to);
    }

    public static short ReadInt16(byte[] from ,int pos){
        byte[] toByte = new byte[2];
        short to = 0;
        if(from.length > pos) {
            System.arraycopy(from, pos, toByte, 0, 2);
            ByteBuffer bb = ByteBuffer.wrap(toByte);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            while (bb.hasRemaining()) {
                to = bb.getShort();
            }
        }
        else {
            Log.d("ReadInt16","From array length is smaller than pos, pos = " + pos + " , length = " + from.length);
        }
        return(to);
    }

    public static int ReadInt32(byte[] from ,int pos){
        byte[] toByte = new byte[4];
        int to = 0;
        if(from.length > pos) {
            System.arraycopy(from, pos, toByte, 0, 4);
            ByteBuffer bb = ByteBuffer.wrap(toByte);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            while (bb.hasRemaining()) {
                to = bb.getInt();
            }
        }
        else {
            Log.d("ReadInt32","From array length is smaller than pos, pos = " + pos + " , length = " + from.length);
        }
        return(to);
    }

    public static int ReadArray(byte[] from, byte[] to, int pos, int len){
        System.arraycopy(from, pos, to, 0, len);
        return len;
    }

    public static float ReadFloat(byte[] from, int pos){
        float to=0;
        byte[] toByte = new byte[4];
        if(from.length > pos) {
            System.arraycopy(from, pos, toByte, 0, 4);
            ByteBuffer bb = ByteBuffer.wrap(toByte);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            while (bb.hasRemaining()) {
                to = bb.getFloat();
            }
        }
        else {
            Log.d("ReadFloat","From array length is smaller than pos, pos = " + pos + " , length = " + from.length);
        }
        return(to);
    }

/////////////////////////////////////////////////////////////////////////////
//		General
/////////////////////////////////////////////////////////////////////////////
public static SubType GetCorespondingResponse(SubType request){
        switch(request){
            case kStart:
                return SubType.kStarted;

            case kStop:
                return SubType.kStopped;

            case kReady: return SubType.kReadyResponse;

            case kSelfTest:
                return SubType.kReturnSelfTestRes;

            case kGetOverlayId:
                return SubType.kReturnOverlayId;

            case kGetOverlayTemp:
                return SubType.kReturnOverlayTemp;

            case kGetOverlayLife:
                return SubType.kReturnOverlayLife;

            case kSetOverlayLife:
                return SubType.kSetOverlayLifeResponse;

            case kGetOverlayLut:
                return SubType.kReturnOverlayLut;

            case kGetOverlaySize:
                return SubType.kReturnOverlaySize;

            case kMeasureSpecificArea:
                return SubType.kEmptySubType;

            case kGetAcFreq:
                return SubType.kReturnAcFreq;

            case kGetOverlayManDate:
                return SubType.kReturnOverlayManDate;

            case kPeriodicTest:
                return SubType.kReturnPeriodicTestResults;

            case kGetHwcVersion:
                return SubType.kReturnHwcVersion;

            case kGetCellSize:
                return SubType.kReturnCellSize;

            case kGetAdcFactor:
                return SubType.kReturnAdcFactor;

            case kGotOverlayParams:
                return SubType.kGotOverlayParamsResponse;

            case kGetOverlayVersion:
                return SubType.kReturnOverlayVersion;

            case kGetRl:
                return SubType.kReturnRl;

            case kGetCp:
                return SubType.kReturnCp;

            case kGetAcPk2Pk:
                return SubType.kReturnAcPk2Pk;

            case kGetFirstActDate:
                return SubType.kReturnFirstActDate;

            case kGetChecksum:
                return SubType.kReturnCheckSum;

            case kSetFirstActDate:
                return SubType.kSetFirstActDateResponse;


            case kRequestOlayEepromCurrentData:
                return SubType.kReturnOlayEepromCurrentData;


            case kGotAllHwcParams:
                return SubType.kGotAllHwcParamsResponse;


            case kGotoHwcParams:
                return SubType.kGotoHwcParamsResponse;

            case kGetConstAmp:
                return SubType.kReturnConstAmp;

            case kGetConstCap:
                return SubType.kReturnConstCap;

            case kReset:
                return SubType.kResetResponse;

            case kKeepAlive:
                return SubType.kKeepAliveResponse;

            case kOlayEepromBurnData:
                return SubType.kOlayEepromBurnDataResponse;

            case kGetHwcParamsAtOnce:
                return SubType.kGetHwcParamsAtOnceResponse;


            case kGetOvcParamsAtOnce:
                return SubType.kGetOvcParamsAtOnceResponse;

            case kGetLutAtOnce:
                return SubType.kGetLutAtOnceResponse;

            case kUpgradeHwcVerified:
                return SubType.kUpgradeHwcVerifiedAck;

            case kUpgradeHwc:
                return SubType.kUpgradeHwcAck;

            case kStartImageTransferSession: 	return SubType.kStartImageTransferSessionResponse;
            case kEndImageTransferSession: 	return SubType.kEndImageTransferSessionResponse;
            case kCheckUpgradeValidity: 		return SubType.kCheckUpgradeValidityResponse;
            case kStartImageTransfer:		return SubType.kStartImageTransferResponse;
            case kSendNextImageBlock:		return SubType.kSendNextImageBlockResponse;
            case kEndImageTransfer:		return SubType.kEndImageTransferResponse;

            ///////////////////			added all subtypes for safety
            case kUpgradeHwcAck:
            case kUpgradeHwcVerifiedAck:

            case kGetHwcParamsAtOnceResponse:
            case kGetOvcParamsAtOnceResponse:
            case kGetLutAtOnceResponse:
            case KGotEventResponse:
            case kReturnConstAmp:
            case kReturnConstCap:
            case kResetResponse:
            case kReturnOlayEepromCurrentData:
            case kGotAllHwcParamsResponse:
            case kGotoHwcParamsResponse:
            case kKeepAliveResponse:
            case kOlayEepromBurnDataResponse:
            case kEmptySubType:
            case kError:
            case kLogMessage:
            case kReturnOverlayId:
            case kReturnOverlayTemp:
            case kReturnOverlayLife:
            case kReturnOverlayLut:
            case kReturnOverlaySize:
            case kReturnAcFreq:
            case kReturnOverlayManDate:
            case kOverlayDisconnected:
            case kOverlayReconnected:
            case kReturnRow:
            case kReturnSelfTestRes:
            case kReturnHwcVersion:
            case kReturnCellSize:
            case kReturnAdcFactor:
            case kStarted:
            case kStopped:
            case kReturnPeriodicTestResults:
            case kReturnOverlayVersion:
            case kReturnRl:
            case kReturnCp:
            case kReturnAcPk2Pk:
            case kReturnFirstActDate:
            case kReturnCheckSum:
            case kGotOverlayParamsResponse:
            case kSetOverlayLifeResponse:
            case kSetFirstActDateResponse:
            case kStartImageTransferSessionResponse:
            case kEndImageTransferSessionResponse:
            case kCheckUpgradeValidityResponse:
            case kStartImageTransferResponse:
            case kSendNextImageBlockResponse:
            case kEndImageTransferResponse:
            case kReadyResponse:
                return SubType.kEmptySubType;

            case kPowerStatus:
                break;
            case kPowerStatusResponse:
                break;
            case kPowerPlugged:
                break;
            case kPowerUnplugged:
                break;
            case kPowerConfig:
                break;
            case kPowerConfigResponse:
                break;
            case kLastSubType:
                break;
        };
        return SubType.kEmptySubType;
    }
}
