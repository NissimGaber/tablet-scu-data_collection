package com.example.nissim.myfirstapplication;

import android.content.Context;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


/**
 * Created by nissim on 2/24/2016.
 */
public class WSScuCommPkg implements WSScuCommPkgInterface {
    private static  WSScuCommPkg instance = null;
    /**
     * holds the msg number of the outgoing messages
     */
    byte   _outReqMsgNumber;
    byte   _outRespMsgNumber;
    byte[] outReqMsgBuff;
    /**
     * holds the msg number of the incoming messages
     */
    byte   _inReqMsgNumber;
    byte   _inRespMsgNumber;
    byte[] inRespMsgBuff;

    /**
     *  container for the data coming throw the comm
     */
    static short  pVal[];
    static byte   pAmp[];

    WSCommPkgDefs.SubType lastMsgSubType;
    WSCommPkgDefs.SubType _nextRequestMsgNumber;
    private WSUsbCommunicationManager usbCommunicationManager;
    private WSPsmProxy psmProxy;
    private WSCommPkgDefs.OvcParams ovcParams;

    protected WSScuCommPkg(Context context) {
        usbCommunicationManager = WSUsbCommunicationManager.getInstance(context);
        usbCommunicationManager.register(this);
        _outReqMsgNumber = 0;
        _outRespMsgNumber = 0;
        _nextRequestMsgNumber = WSCommPkgDefs.SubType.kEmptySubType;
        outReqMsgBuff = new byte[128];
        _inReqMsgNumber = 0;
        _inRespMsgNumber = 0;
        inRespMsgBuff = new byte[128];
    }

    public static WSScuCommPkg getInstance(Context context) {
        if(instance == null) {
            instance = new WSScuCommPkg(context);
        }
        return instance;
    }

    public void register(WSPsmProxy callback) {
        psmProxy = callback;
    }

    public void StartOperation() {
        boolean res = usbCommunicationManager.connect();
        if (res) {
            Log.d("StartOperation", "Port Openned successfully");
        } else {
            Log.d("StartOperation", "Port Open Failed");
        }
    }

    // Determines the number for the type of message that the scu sends to the peer
    protected byte GetNextMsgNumber(WSCommPkgDefs.Category cat) {
        byte temp;
        if (cat == WSCommPkgDefs.Category.kEventNoResponse)
        // Arbitrary - should not be used in a real reply message
        {
            Log.d("GetNextMsgNumber","msgNumber = 0");
            return 0;
        } else if (cat == WSCommPkgDefs.Category.kResponse) {
            temp = _outRespMsgNumber;
            _outRespMsgNumber = (byte) ((_outRespMsgNumber + 1) % WSCommPkgDefs.kFrameCountSize);
            Log.d("GetNextMsgNumber","msgNumber = " + _outRespMsgNumber);
            return temp;
        } else {
            // sending a request to the peer
            temp = _outReqMsgNumber;
            _outReqMsgNumber = (byte) ((_outReqMsgNumber + 1) % WSCommPkgDefs.kFrameCountSize);
            Log.d("GetNextMsgNumber","msgNumber = " + _outRespMsgNumber);
            return temp;
        }
    }

    void SendHwcParamsAtOnce() {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kGetHwcParamsAtOnce, outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kGetHwcParamsAtOnce;
    }

    //*********** REQUESTS ********************
    /////////////////////////////////////////////////////////
//		Requests
/////////////////////////////////////////////////////////

    protected boolean SendRequest(WSCommPkgDefs.SubType subType) {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, subType,outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = subType;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    boolean GetHwcParamsAtOnce() {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kGetHwcParamsAtOnce,outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kGetHwcParamsAtOnce;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    boolean RequestGotAllHwcParams() {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kGotAllHwcParams,outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kGotAllHwcParams;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    void Ready() {

        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kReady, outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kReady;
        //Log.d("readMessageCallback","outReqMsgBuff = " + outReqMsgBuff);
        //Log.d("readMessageCallBack","Data: 1 = "+outReqMsgBuff[0]+", 2 = "+outReqMsgBuff[1]+", 3 = "+outReqMsgBuff[2]+", 4 = "+outReqMsgBuff[3]+" , 5 = "+outReqMsgBuff[4]+" , 6 = "+outReqMsgBuff[5]+" , 7 = "+outReqMsgBuff[6]+" , 8 = "+outReqMsgBuff[7]+" , 9 = "+outReqMsgBuff[8]);

        usbCommunicationManager.send(outReqMsgBuff);
    }

    boolean SelfTest() {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kSelfTest,outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kSelfTest;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    boolean IsOvcDataValid()
    {
        return SendRequest(WSCommPkgDefs.SubType.kGetChecksum);
    }

    boolean GetOvcParamsAtOnce()
    {
        return SendRequest(WSCommPkgDefs.SubType.kGetOvcParamsAtOnce);
    }

    boolean GetLutAtOnce()
    {
        return SendRequest(WSCommPkgDefs.SubType.kGetLutAtOnce);
    }

    boolean GotOvcParams()
    {
        return SendRequest(WSCommPkgDefs.SubType.kGotOverlayParams);
    }

    boolean StartMeasuring(float freq)
    {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kStart, outReqMsgBuff);
        len += WSCommPkgUtils.WriteFloat(freq, outReqMsgBuff, msgTotalLen+WSCommPkgDefs.kMsgLengthSize);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kStart;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    boolean SetPowerConfig(float refreshRate, float batteryImageDelay, float lowBatteryImageDelay)
    {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int temp = 0;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kPowerConfig, outReqMsgBuff);
        temp = WSCommPkgUtils.WriteFloat(refreshRate, outReqMsgBuff, msgTotalLen + WSCommPkgDefs.kMsgLengthSize);
        len += temp;
        msgTotalLen += temp;
        temp = WSCommPkgUtils.WriteFloat(batteryImageDelay,outReqMsgBuff, msgTotalLen + WSCommPkgDefs.kMsgLengthSize);
        len += temp;
        msgTotalLen += temp;
        temp = WSCommPkgUtils.WriteFloat(lowBatteryImageDelay,outReqMsgBuff, msgTotalLen + WSCommPkgDefs.kMsgLengthSize);
        len += temp;
        msgTotalLen += temp;
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kPowerConfig;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    boolean SetPowerStatus(WSCommPkgDefs.PowerStatus status)
    {
        short len = WSCommPkgDefs.kMsgRequestPostLengthHeaderSize;
        int msgTotalLen = 0;

        msgTotalLen = WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kRequest), WSCommPkgDefs.Category.kRequest, WSCommPkgDefs.SubType.kPowerStatus, outReqMsgBuff);
        len += WSCommPkgUtils.WriteInt8(status.getId(), outReqMsgBuff, msgTotalLen + WSCommPkgDefs.kMsgLengthSize);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(len, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.kPowerStatus;
        usbCommunicationManager.send(outReqMsgBuff);
        return true;
    }

    protected void SendGotEventReply()
    {

        int msgTotalLen = 0;
        //the only two relevant fields are category and ack frame number
        msgTotalLen =  WSCommPkgUtils.WriteGeneralHeader(GetNextMsgNumber(WSCommPkgDefs.Category.kResponse), WSCommPkgDefs.Category.kResponse, WSCommPkgDefs.SubType.KGotEventResponse, outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgStatus(WSCommPkgDefs.Status.kSuccess, outReqMsgBuff);
        msgTotalLen += WSCommPkgUtils.WriteMsgLength(WSCommPkgDefs.kMsgResponsePostLengthHeaderSize, outReqMsgBuff);
        lastMsgSubType = WSCommPkgDefs.SubType.KGotEventResponse;
        usbCommunicationManager.send(outReqMsgBuff);
        Log.d("SendGotEventReply","Reply sent to the HWC for getting Event");
    }

    //*********** RESPONSES ********************
    public void readMessageCallBack(byte inMsg[]) {
        if(inMsg.length > 0) {
            //read message header details
            WSCommPkgDefs.Category cat;
            WSCommPkgDefs.SubType subType;
            WSCommPkgDefs.Status msgStatus;

            cat = WSCommPkgUtils.ReadMsgCategory(inMsg);
            subType = WSCommPkgUtils.ReadMsgSubType(inMsg);

            //Log.d("readMessageCallBack", "message length = " + inMsg.length);
            //Log.d("readMessageCallBack", "category = " + cat.toString());
            //Log.d("readMessageCallBack", "MsgType = " + subType. toString());

            byte[] toByte = new byte[4];
            int msgFlag = 0;
            System.arraycopy(inMsg, 0, toByte, 0, 4);
            ByteBuffer bb = ByteBuffer.wrap(toByte);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            while (bb.hasRemaining()) {
                msgFlag = bb.getInt();
            }
            //Log.d("readMessageCallback", "msgFlag = " + msgFlag);
            //Log.d("readMessageCallBack", "Data: 1 = " + inMsg[0] + ", 2 = " + inMsg[1] + ", 3 = " + inMsg[2] + ", 4 = " + inMsg[3] + " , 5 = " + inMsg[4] + " , 6 = " + inMsg[5] + " , 7 = " + inMsg[6] + " , 8 = " + inMsg[7] + " , 9 = " + inMsg[8]);
            if (cat == WSCommPkgDefs.Category.kResponse) {

                WSCommPkgDefs.SubType expectedResponseSubtype = WSCommPkgUtils.GetCorespondingResponse(lastMsgSubType);
                int buffPtrPos = WSCommPkgDefs.kMsgResponseHeaderSize;//kMsgResponsePostLengthHeaderSize;
                msgStatus = WSCommPkgUtils.ReadMsgStatus(inMsg);
                //Log.d("readMessageCallBack", "msgStatus = " + msgStatus.toString());

                if (subType == WSCommPkgDefs.SubType.kReadyResponse) {
                    //Log.d("readMessageCallback", "Ready Response recieved, sending self test");
                    //SelfTest();
                    Log.d("readMessageCallback", "Ready Response recieved, sending Power Config");
                    SetPowerConfig(2.0f,1.0f,10.0f);
                }
                if (subType == WSCommPkgDefs.SubType.kPowerConfigResponse) {
                    Log.d("readMessageCallback", "Power Config Response recieved, sending Power Status");
                    SetPowerStatus(WSCommPkgDefs.PowerStatus.kAcPowerStatus);
                }
                if (subType == WSCommPkgDefs.SubType.kPowerStatusResponse) {
                    Log.d("readMessageCallback", "Power Status Response recieved, sending self test");
                    SelfTest();
                }
                if(subType == WSCommPkgDefs.SubType.kReturnSelfTestRes) {
                    Log.d("readMessageCallback", "Self Test Response Recieved, sending Get HWC params");
                    GetHwcParamsAtOnce();
                }
                if(subType == WSCommPkgDefs.SubType.kGetHwcParamsAtOnceResponse) {
                    Log.d("readMessageCallback", "GetHwcParamsAtOnceResponsee recieved, sending GetHwcParamsAtOnceResponse recieved");
                    ParseHwcParams(msgStatus, inMsg, buffPtrPos);
                    RequestGotAllHwcParams();
                }
                if(subType == WSCommPkgDefs.SubType.kGotAllHwcParamsResponse) {
                    Log.d("readMessageCallback", "GotAllHwcParamsResponse recieved, sending IsOvcDataValid");
                    IsOvcDataValid();
                }
                if(subType == WSCommPkgDefs.SubType.kReturnCheckSum) {
                    Log.d("readMessageCallback", "ReturnCheckSum recieved");
                    if(msgStatus == WSCommPkgDefs.Status.kSuccess) {
                        byte check = WSCommPkgUtils.ReadInt8(inMsg, buffPtrPos);
                        buffPtrPos += 1;
                        Log.d("readMessageCallback", "ReturnCheckSum , result = " + check + ", Send GetOvcParams");
                        GetOvcParamsAtOnce();
                    }
                    else {
                        Log.d("readMessageCallback", "ReturnCheckSum is not success, Status = " + msgStatus.toString());
                    }
                }
                if(subType == WSCommPkgDefs.SubType.kGetOvcParamsAtOnceResponse) {
                    Log.d("readMessageCallback", "GetOvcParamsAtOnceResponsee recieved, sending GetHwcParamsAtOnceResponse recieved");
                    ParseOvcParams(msgStatus, inMsg, buffPtrPos);
                    GetLutAtOnce();
                }
                if(subType == WSCommPkgDefs.SubType.kGetLutAtOnceResponse) {
                    Log.d("readMessageCallback", "kGetLutAtOnceResponse recieved");
                    ParseLutParams(msgStatus, inMsg, buffPtrPos);
                    GotOvcParams();
                }
                if(subType == WSCommPkgDefs.SubType.kGotOverlayParamsResponse) {
                    Log.d("readMessageCallback", "GetOverlayParamsResponsee recieved");
                    Log.d("readMessageCallback", "sending StartMeasuring ");
                    StartMeasuring((float) 2.0);
                }
                if(subType == WSCommPkgDefs.SubType.kStarted) {
                    Log.d("readMessageCallback","******************* START READING DATA ****************");
                }

            }
            else {
                if(cat == WSCommPkgDefs.Category.kEvent || cat == WSCommPkgDefs.Category.kEventNoResponse) {
                    _nextRequestMsgNumber = WSCommPkgDefs.SubType.kEmptySubType;
                    //Log.d("readMessageCallback", "Got event = " + subType. toString());
                    short buffPtrPos = WSCommPkgDefs.kMsgEventHeaderSize;/*kMsgEventPostLengthHeaderSize;*/
                    if(subType == WSCommPkgDefs.SubType.kOverlayDisconnected) {
                        Log.d("readMessageCallback", "OVC Disconnected");
                        psmProxy.PsmConnectedReceived();
                    }
                    if(subType == WSCommPkgDefs.SubType.kOverlayReconnected) {
                        _nextRequestMsgNumber = WSCommPkgDefs.SubType.kGetChecksum;
                        Log.d("readMessageCallback", "OVC Recconnected");
                        psmProxy.PsmDisconnectedReceived();
                    }
                    if(subType == WSCommPkgDefs.SubType.kReturnRow) {
                        //og.d("readMessageCallback", "Row Recieved");
                        ParseRowEvent(inMsg,buffPtrPos);

                    }
                    if (cat == WSCommPkgDefs.Category.kEvent) {
                        Log.d("readMessageCallback","Got Event send response");
                        SendGotEventReply();
                    }
                    if(_nextRequestMsgNumber != WSCommPkgDefs.SubType.kEmptySubType) {
                        SendRequest(_nextRequestMsgNumber);
                        _nextRequestMsgNumber = WSCommPkgDefs.SubType.kEmptySubType;
                    }
                }
            }
        }
        else {
            Log.d("readMessageCallBack", "Read Empty data");
        }
    }
    void ParseHwcParams(WSCommPkgDefs.Status status,byte buff[],int buffPtrPos) {

        WSCommPkgDefs.HwcParams pParams = new WSCommPkgDefs.HwcParams();


        if (status == WSCommPkgDefs.Status.kSuccess) {

            float floatData=0;
            short shortData=0;
            int   intData=0;
            pParams.adcFactor = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "adcFactor = " + pParams.adcFactor );

            pParams.acFreq = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "acFreq = " + pParams.acFreq );

            pParams.acPk2Pk = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseHwcParams", "acPk2Pk = " + pParams.acPk2Pk );

            pParams.rload = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "rload = " + pParams.rload);

            pParams.cPar = WSCommPkgUtils.ReadFloat(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "cPar = " + pParams.cPar);

            pParams.cConst = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            //pParams.cConst *= (float)Math.pow((double)10,-12.0);
            Log.d("ParseHwcParams", "cConst = " + pParams.cConst );

            pParams.constAmp = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "constAmp = " + pParams.constAmp);

            pParams.hwcVersionMajor = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseHwcParams", "hwcVersionMajor = " + pParams.hwcVersionMajor );

            pParams.hwcVersionMinor = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseHwcParams", "hwcVersionMinor = " + pParams.hwcVersionMajor);

            pParams.hwcVersionBuild = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseHwcParams", "hwcVersionBuild = " + pParams.hwcVersionBuild);

            pParams.hwVersion = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "hwcVersion = " + pParams.hwVersion);

            pParams.constCapColumn = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "constCapColumn = " + pParams.constCapColumn);

            pParams.constCapValue = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "constCapValue = " + pParams.constCapColumn);

            pParams.maxRowIndex = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "maxRowIndex = " + pParams.maxRowIndex);

            pParams.maxColumnIndex = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "maxColumnIndex = " + pParams.maxColumnIndex);

            pParams.constAmpLevel = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "constAmpLevel = " + pParams.constAmpLevel);

            pParams.acAmpMin = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "acAmpMin = " + pParams.acAmpMin);

            pParams.acAmpMax = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "acAmpMax = " + pParams.acAmpMax);

            pParams.acFreqMin = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "acFreqMin = " + pParams.acFreqMin);

            pParams.acFreqMax = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseHwcParams", "acFreqMax = " + pParams.acFreqMax);
            psmProxy.HwcParamsReceived(pParams);
        }
    }

    void ParseOvcParams(WSCommPkgDefs.Status status,byte buff[],int buffPtrPos) {
        if(ovcParams == null) {
            ovcParams = new WSCommPkgDefs.OvcParams();
        }

        if (status == WSCommPkgDefs.Status.kSuccess) {

            ovcParams.overlayVersionMajor = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseOvcParams", "overlayVersionMajor = " + ovcParams.overlayVersionMajor );

            ovcParams.overlayVersionMinor = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            Log.d("ParseOvcParams", "overlayVersionMinor = " + ovcParams.overlayVersionMinor );

            ovcParams.overlayCellSizeX = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
            buffPtrPos += 1;
            Log.d("ParseOvcParams", "overlayCellSizeX = " + ovcParams.overlayCellSizeX );

            ovcParams.overlayCellSizeY = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
            buffPtrPos += 1;
            Log.d("ParseOvcParams", "overlayCellSizeY = " + ovcParams.overlayCellSizeY );

            ovcParams.overlayId = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseOvcParams", "overlayId = " + ovcParams.overlayId );

            ovcParams.overlayLife = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseOvcParams", "overlayLife = " + ovcParams.overlayLife);

            ovcParams.overlaySizeCols = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
            buffPtrPos += 1;
            Log.d("ParseOvcParams", "overlaySizeCols = " + ovcParams.overlaySizeCols);

            ovcParams.overlaySizeRows = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
            buffPtrPos += 1;
            Log.d("ParseOvcParams", "overlaySizeRows = " + ovcParams.overlaySizeRows);

            ovcParams.overlayManufacturingDate = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseOvcParams", "overlayManufacturingDate = " + ovcParams.overlayManufacturingDate );

            ovcParams.overlayFirstActivationDate = WSCommPkgUtils.ReadInt32(buff, buffPtrPos);
            buffPtrPos += 4;
            Log.d("ParseOvcParams", "overlayFirstActivationDate = " + ovcParams.overlayFirstActivationDate);
            psmProxy.OvcParamsReceived(ovcParams);
            if(pVal == null || ovcParams.overlaySizeCols != pVal.length) {
                pVal = new short[ovcParams.overlaySizeCols+1]; // need to add one for the reference column
                pAmp = new byte[ovcParams.overlaySizeCols+1]; // need to add one for the reference column
            }

        }
    }

    void ParseLutParams(WSCommPkgDefs.Status status,byte buff[],int buffPtrPos) {

        WSCommPkgDefs.Lut pLut = new WSCommPkgDefs.Lut();


        if (status == WSCommPkgDefs.Status.kSuccess) {


            ///////////////
            //    LUT    //
            ///////////////
            short numOfRegions;

            numOfRegions = WSCommPkgUtils.ReadInt16(buff, buffPtrPos);
            buffPtrPos += 2;
            pLut.numOfRegions = numOfRegions;
            Log.d("ParseLut","numOfRegions = " + numOfRegions);

            for (short i=0 ; i<numOfRegions ; ++i) {

                pLut.regionsList[i].lowerBound = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
                Log.d("ParseLut","lowerBound = "+pLut.regionsList[i].lowerBound);
                buffPtrPos += 4;
                pLut.regionsList[i].upperBound = WSCommPkgUtils.ReadFloat(buff,buffPtrPos);
                Log.d("ParseLut","upperBound = "+pLut.regionsList[i].upperBound);
                buffPtrPos += 4;

                for (short j=0; j<WSCommonDefs.kLutMaxCoefNum ; ++j) {
                    pLut.regionsList[i].coefList[j] = WSCommPkgUtils.ReadFloat(buff, buffPtrPos);
                    buffPtrPos += 4;
                }
                String printStr = new String();
                for (short j=0; j<WSCommonDefs.kLutMaxCoefNum ; ++j) {
                    printStr += pLut.regionsList[i].coefList[j];
                    printStr += " , ";
                }
                Log.d("ParseLut", printStr);

            }
            psmProxy.LutDataReceived(pLut,numOfRegions);
        }
    }
    void ParseRowEvent(byte buff[],int buffPtrPos) {
        byte rowNumber;
        byte len;

        //read the row number
        rowNumber = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
        buffPtrPos += 1;

        //read the length of the arrays of the measurements
        len = WSCommPkgUtils.ReadInt8(buff, buffPtrPos);
        buffPtrPos += 1;
        Log.d("ParseRowEvent","Row Number " + rowNumber + " , len = " + len);
        for(int i=0;i<len;i++)
        {
            pVal[i] = WSCommPkgUtils.ReadInt16(buff,(buffPtrPos + (i*2)));
            pAmp[i] = WSCommPkgUtils.ReadInt8(buff, (buffPtrPos + i + (len * 2)));
        }
        psmProxy.RowDataReceived(pVal, pAmp, rowNumber, len);
    }
}
