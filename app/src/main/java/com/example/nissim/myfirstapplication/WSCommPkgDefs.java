package com.example.nissim.myfirstapplication;

import android.util.Log;

/**
 * Created by nissim on 2/24/2016.
 */
public class    WSCommPkgDefs {

    /*
     * 	    MsgFlag        MsgFrameNum 	   MsgLength	Category	SubType		<Status>
     * 	    4 bytes         1Byte			2Bytes		1Byte		1Byte		1Byte
     * |_______________________________________________________________________________|
     * 			Request & Event
     * |_____________________________________________________________________________________________|
     * 			Response
     */

    /**
     * the msg flag presented in the first 4 bytes of every msg and used for framing
     */
        public static final int kMsgFlag = 0xaabbbbaa;

    /**
     * the size in bytes of the flag part of the message
     */
        public static final byte kMsgFlagSize = 4; //in Bytes

    /**
     * the size in bytes of the frame number part of the message
     */
        public static final byte kMsgFrameNumSize = 1; //in Bytes

    /**
     * the size in bytes of the length part of the message
     */
        public static final byte kMsgLengthSize = 2; //in Bytes

    /**
     * the size in bytes of the Category part of the message
     */
        public static final byte kMsgCategorySize = 1; //in Bytes

    /**
     * the size in bytes of the SubType part of the message
     */
        public static final byte kMsgSubTypeSize = 1; //in Bytes

    /**
     * the size in bytes of the Status part of the message
     */
        public static final byte kMsgStatusSize = 1;  //in Bytes

    ////////////////////////////////////////////////////////////////////////
    //	Header sizes are in Byes and do not include the MsgLength it self.
    ////////////////////////////////////////////////////////////////////////
    /**
     * Header size of a Request message in bytes - without the field of the message length and the frame number.
     */
        public static final byte kMsgRequestPostLengthHeaderSize =   kMsgCategorySize + kMsgSubTypeSize;					 	//in Bytes

    /**
     * Header size of a Event message in bytes - without the field of the message length and the frame number.
     */
        public static final byte kMsgEventPostLengthHeaderSize =   kMsgCategorySize + kMsgSubTypeSize;	 						//in Bytes

    /**
     * Header size of a response message in bytes - without the field of the message length and the frame number.
     */
        public static final byte kMsgResponsePostLengthHeaderSize =   kMsgCategorySize + kMsgSubTypeSize + kMsgStatusSize; 	//in Bytes

    /**
     * Header size of a response message in bytes - without the field of the message length and the frame number.
     */
        public static final byte kMsgPreLengthSize = kMsgFlagSize + kMsgFrameNumSize + kMsgLengthSize; 	//in Bytes

    /**
     * the total header size of the request msg
     */
        public static final byte kMsgRequestHeaderSize =   kMsgRequestPostLengthHeaderSize + kMsgPreLengthSize;

    /**
     *  the total header size of the response msg
     */
        public static final byte kMsgResponseHeaderSize =   kMsgResponsePostLengthHeaderSize + kMsgPreLengthSize;

    /**
     *  the total header size of the event msg
     */
        public static final byte kMsgEventHeaderSize =   kMsgEventPostLengthHeaderSize + kMsgPreLengthSize;

    ////////////////////////////////////////////////////////////////////////
    //	Positions of the fields in the message
    ////////////////////////////////////////////////////////////////////////

    /**
     * The position of the flag field in the message
     */
        public static final byte kPosMsgFlag = 0;

    /**
     * The position of the length field in the message
     */
        public static final byte kPosMsgFrameNum = kMsgFlagSize;

    /**
     * The position of the length field in the message
     */
        public static final byte kPosMsgLength = kPosMsgFrameNum + kMsgFrameNumSize;

    /**
     * The position of the Category field in the message
     */
        public static final byte kPosMsgCategory = kPosMsgLength + kMsgLengthSize;

    /**
     * The position of the SubType field in the message
     */
        public static final byte kPosMsgSubType = kPosMsgCategory + kMsgCategorySize;		//4

    /**
     * The position of the Status field in the message
     */
        public static final byte kPosMsgStatus = kPosMsgSubType + kMsgSubTypeSize;			//5
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * used to define the max size of the ab buff of the USB
     */
        public static final byte kMaxUsbAbBufSize = 64;

    /**
     *	max buffer size for the In buffer
     */
        public static final byte kMaxInPktSize = 64;

    /**
     *	max buffer size for the OUT buffer
     */
        public static final byte kMaxOutPktSize = 64;

    /**
     * the size defined by the USB protocol as the max buffer size for a bulk transfer
     */
        public static final byte kMaxBulkBuffSize = 64;

    /**
     * the maximum size of an OUT msg in bytes.
     */
        public static final short kMaxOutMsgSize = 550;

    /**
     * the  size of an OUT buffer in bytes.
     */
        public static final short kOutBuffSize = kMaxOutMsgSize + kMsgResponsePostLengthHeaderSize + kMsgPreLengthSize;

    /**
     * the maximum size of an IN msg in bytes.
     */
        public static final short kMaxInMsgSize = 550;

    /**
     * the  size of an OUT buffer in bytes.
     */
        public static final short kInBuffSize = kMaxInMsgSize + kMsgResponsePostLengthHeaderSize + kMsgPreLengthSize;

    /**
     * defines the max frame count.
     */
        public static final short kFrameCountSize = 256;

    /**
     * the maximum log size in Bytes
     */
        public static final  int kMaxLogMsgSize = 550; //in bytes



    ////////////////////////////////////////////////////////////////////
    // USB host definitions
    ////////////////////////////////////////////////////////////////////

    /**
     * The timeout in milliseconds for incoming messages. Defined according to the SCU.
     */
        public static final int kInTimeout = 2000; //	2000 msec



    ////////////////////////////////////////////////////////////////////
    // USB host definitions
    ////////////////////////////////////////////////////////////////////

        public static class LutRegion{
            float lowerBound;
            float upperBound;
            float[] coefList;

            public LutRegion() {
                Log.d("LutRegion", "Create new float array");
                coefList = new float[WSCommonDefs.kLutMaxCoefNum];
            }
        }

        public static class Lut{
            short numOfRegions;
            LutRegion[] regionsList;
            public Lut() {
                Log.d("Lut", "Create new Lut Region");
                regionsList = new LutRegion[WSCommonDefs.kMaxNumLutRegions];
                for(int i=0;i<WSCommonDefs.kMaxNumLutRegions;i++) {
                    regionsList[i] = new LutRegion();
                }
            }
        }

        public static class OvcParams{
            short overlayVersionMajor;
            short overlayVersionMinor;
            byte overlayCellSizeX;
            byte overlayCellSizeY;
            int overlayId;
            int overlayLife;
            byte overlaySizeCols;
            byte overlaySizeRows;
            int overlayManufacturingDate;
            int overlayFirstActivationDate;

        }

        public static class HwcParams{
            float adcFactor;
            float acFreq;
            short acPk2Pk;
            int rload;
            float cPar;
            float cConst;
            float constAmp;

            short hwcVersionMajor;
            short hwcVersionMinor;
            short hwcVersionBuild;

            //HW params
            int hwVersion;
            int constCapColumn;
            float constCapValue;
            int maxRowIndex;
            int maxColumnIndex;
            float constAmpLevel;
            float acAmpMin;
            float acAmpMax;
            float acFreqMin;
            float acFreqMax;
        }


        public static class HwcPowerConfig{
            float refreshRate;
            float batteryImageDelay;
            float LowBatteryImageDelay;
        }
        /**
         * Category emun - the categories of the messages over the USB protocol
         */
        public enum Category{
            kEventNoEvent((byte)0),
            kRequest((byte)1),
            kResponse((byte)2),
            kEvent((byte)3),
            kEventNoResponse((byte)4);
            private final byte id;
            Category(byte i) {
                this.id = i;
            }
            public byte getId() {
                return this.id;
            }
        }

    // TODO - CR (Future) - Create separate enumerators per request, response, and event.
        /**
         * Subtype emun - includes all the subtypes of messages.
         */
        public enum SubType{
            kEmptySubType((byte)0)        ,//		= 0,
            kStart ((byte)1)				    ,//		= 1,
            kStop((byte)2)  				    ,//		= 2,
            kSelfTest((byte)3)			    ,//		= 3,
            kGetOverlayId((byte)4) 			,//		= 4,
            kGetOverlayTemp((byte)5) 		,//		= 5,
            kGetOverlayLife((byte)6)		,//		= 6,
            kSetOverlayLife((byte)7) 		,//		= 7,
            kGetOverlayLut((byte)8) 			,//		= 8,
            kGetOverlaySize((byte)9) 		,//		= 9,
            kMeasureSpecificArea((byte)10) 	,//	    = 10,
            kGetAcFreq((byte)11) 			    ,//		= 11,
            kGetOverlayManDate((byte)12) 		,//		= 12,
            kPeriodicTest((byte)13) 			,//		= 13,
            kError((byte)14) 				    ,//		= 14,
            kLogMessage((byte)15) 			,//		= 15,
            kReturnOverlayId((byte)16) 		,//		= 16,
            kReturnOverlayTemp((byte)17) 		,//		= 17,
            kReturnOverlayLife((byte)18) 		,//		= 18,
            kReturnOverlayLut((byte)19) 		,//		= 19,
            kReturnOverlaySize((byte)20) 		,//		= 20,
            kReturnAcFreq((byte)21) 			,//		= 21,
            kReturnOverlayManDate((byte)22)	,// 	= 22,
            kOverlayDisconnected((byte)23) 	,// 	= 23,
            kOverlayReconnected((byte)24) 	,//	    = 24,
            kReturnRow((byte)25)			    ,//		= 25,
            kReturnSelfTestRes((byte)26) 		,//		= 26,
            kGetHwcVersion((byte)27) 			,//		= 27,
            kReturnHwcVersion((byte)28) 		,//		= 28,
            kGetCellSize((byte)29)			,//		= 29,
            kReturnCellSize((byte)30)			,//		= 30,
            kGetAdcFactor((byte)31)			,//		= 31,
            kReturnAdcFactor((byte)32)		,//		= 32,
            kReady((byte)33)				    ,//		= 33,
            kStarted((byte)34)			    ,//		= 34,
            kStopped((byte)35)			    ,//		= 35,
            kGotOverlayParams((byte)36)		,//		= 36,
            kGotOverlayParamsResponse((byte)37)	,//= 37,
            kReturnPeriodicTestResults((byte)38)	,//= 38,
            kGetOverlayVersion((byte)39)		,//	= 39,
            kReturnOverlayVersion((byte)40)		,//= 40,
            kSetOverlayLifeResponse((byte)41)		,//= 41,
            kGetRl((byte)42)				,//		= 42,
            kReturnRl((byte)43)			,//		= 43,
            kGetCp((byte)44)				,//		= 44,
            kReturnCp((byte)45)			,//		= 45,
            kGetAcPk2Pk((byte)46)			,//		= 46,
            kReturnAcPk2Pk((byte)47)			,//	= 47,
            kGetFirstActDate((byte)48)		,//	= 48,
            kReturnFirstActDate((byte)49)		,//	= 49,
            kGetChecksum((byte)50)			,//	= 50,
            kReturnCheckSum((byte)51)			,//	= 51,
            kSetFirstActDate((byte)52)		,//	= 52,
            kSetFirstActDateResponse((byte)53)	,//= 53,
            kReadyResponse((byte)54)			,//	= 54,
            kKeepAliveResponse((byte)55)		,//	= 55,
            kOlayEepromBurnData((byte)56)		,//	= 56,
            kOlayEepromBurnDataResponse((byte)57) 	,//	= 57,
            kRequestOlayEepromCurrentData((byte)58) 	,// = 58,
            kReturnOlayEepromCurrentData((byte)59) 	,//= 59,
            kGotAllHwcParams((byte)60)		,//	= 60,
            kGotAllHwcParamsResponse((byte)61)	,//= 61,
            kGotoHwcParams((byte)62)			,//	= 62,
            kGotoHwcParamsResponse((byte)63)	,//= 63,
            KGotEventResponse((byte)64)		,//	= 64,
            kGetConstAmp((byte)65)			,//	= 65,
            kReturnConstAmp((byte)66)			,//	= 66,
            kGetConstCap((byte)67)			,//	= 67,
            kReturnConstCap((byte)68)			,//	= 68,
            kReset((byte)69)				,//		= 69,
            kResetResponse((byte)70)			,//	= 70,
            kGetHwcParamsAtOnce((byte)71)		,//	= 71,
            kGetHwcParamsAtOnceResponse((byte)72)	,//= 72,
            kGetOvcParamsAtOnce((byte)73)		,//	= 73,
            kGetOvcParamsAtOnceResponse((byte)74)	,//= 74,
            kGetLutAtOnce((byte)75)			,//	= 75,
            kGetLutAtOnceResponse((byte)76)		,//= 76,
            kKeepAlive((byte)77)			,//		= 77,
            kUpgradeHwc((byte)78)                     ,// = 78
            kUpgradeHwcAck((byte)79)                  ,// = 79
            kUpgradeHwcVerified((byte)80)             ,// = 80
            kUpgradeHwcVerifiedAck((byte)81)          ,// = 81
            kStartImageTransferSession((byte)82)         	,// = 82
            kStartImageTransferSessionResponse((byte)83)	,// = 83
            kEndImageTransferSession((byte)84)         	,// = 84
            kEndImageTransferSessionResponse((byte)85) 	,// = 85
            kCheckUpgradeValidity((byte)86)		        ,// = 86
            kCheckUpgradeValidityResponse((byte)87)           ,// = 87
            kStartImageTransfer((byte)88)			,// = 88
            kStartImageTransferResponse((byte)89)		,// = 89
            kSendNextImageBlock((byte)90)			,// = 90
            kSendNextImageBlockResponse((byte)91)		,// = 91
            kEndImageTransfer((byte)92)			,// = 92
            kEndImageTransferResponse((byte)93)		,// = 93
            kPowerStatus((byte)94)                            ,//=94
            kPowerStatusResponse((byte)95)                    ,//=95
            kPowerPlugged((byte)96)                           ,//=96
            kPowerUnplugged((byte)97)                         ,//=97
            kPowerConfig((byte)98)							,//=98
            kPowerConfigResponse((byte)99)					,//=99
            kLastSubType((byte)100), ;
            private final byte id;
            SubType(byte i) {
                this.id = i;
            }
            public byte getId() {
                return this.id;
            }

        }

        enum HwcUpgradeParts {
            kHwcUserCode(0),
            kSecondStageBootLoader(1);
            private final int id;

            HwcUpgradeParts(int i) {
                this.id = i;
            }
        }
        enum PowerStatus {
            kPowerStatusUnknown((byte)-1),
            kAcPowerStatus((byte)0),    //
            kBatteryStatus((byte)1),
            kLowBatteryStatus((byte)2);     //
            private final byte id;
            PowerStatus(byte i) {
                this.id = i;
            }
            public byte getId() {return this.id;}
        }

        /**
         * Status emun - represents different statuses for messages over the comm protocol
         */
        enum Status {
            kSuccess((byte)0),
            kFailed((byte)1),
            kUnknownCommand((byte)2),
            kInvalidParameter((byte)3),
            kTimeout((byte)4),
            kDisconnect((byte)5),
            kInvalidRequest((byte)6),    //will be sent from the HWC when an illegal request was recieved according to the state machine
            kMsgToLongHwc((byte)7),
            kNoCallback((byte)8),
            kGetNextParamOutOfBounds((byte)12),
            kStatus_InternalError((byte)14),

            kImageTooLarge((byte)15),        // The total size of the transferred image blocks exceeds the maximum available size
            kInvalidImageSize((byte)16),    // Transferred image size mismatches the sum of individual blocks
            kInvalidImageChecksum((byte)17),    // Transferred image failed checksum verification

            kFlashProgramError((byte)18),      // IAP failed
            kStatusNoStatus((byte)19);
            private final byte id;

            Status(byte i) {
                this.id = i;

            }
            public byte getId() {
                return this.id;
            }
        }

        enum HwcImageType {
            kHwcImageType_2ndStageBootLoader(0),
            kHwcImageType_UserCode(1),
            kHwcImageType_Invalid(2);
            int id;

            HwcImageType(int i) {
                id = i;
            }
        }

        public static final int kTransferredImageBlockSize = 512;
}