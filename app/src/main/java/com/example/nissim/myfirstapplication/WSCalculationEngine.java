package com.example.nissim.myfirstapplication;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nissim on 4/26/2016.
 */
public class WSCalculationEngine implements WSIRawDataReceiver {
    WSCommPkgDefs.Lut       mpLut;
    int                     mRowNumber;
    static  byte            mPressureRowData[];
    private List<WSICalculatedDataReceiver> listeners = new ArrayList<WSICalculatedDataReceiver>();


    public void RegisterToCalculatedDataReady(WSICalculatedDataReceiver callback)
    {
        listeners.add(callback);
    }

    public void PsmDisconnectedReceived()
    {
        Log.d("WSCalculationEngine", "PSM Disconnected");
    }

    public void PsmConnectedReceived()
    {
        Log.d("WSCalculationEngine", "PSM Connected");
    }

    public void OvcParamsReceived(WSCommPkgDefs.OvcParams ovcParams)
    {
        mPressureRowData = new byte[ovcParams.overlaySizeCols];
    }

    public void SetLutData(WSCommPkgDefs.Lut lut,short numOfRegions)
    {
        mpLut = new WSCommPkgDefs.Lut();
        mpLut = lut;
        for (short i=0 ; i<numOfRegions ; ++i) {
            String printStr = new String();
            for (short j = 0; j < WSCommonDefs.kLutMaxCoefNum; ++j) {
                printStr += mpLut.regionsList[i].coefList[j];
                printStr += " , ";
            }
            Log.d("LutDataReceived", printStr);
        }
    }
    /**********************************************************************************************
     * Calculate the Pressure row data according to a given capacitance row data
     * Send it to the Calculated Data Receiver
     **********************************************************************************************/
    public void NewRawDataReceived(float rowCapacitanceData[], int rowNumber, byte len)
    {
        if(rowNumber == 0 || rowNumber == (mRowNumber+1)) {
            mRowNumber = rowNumber;
            for(int col=0;col<len-1;col++){ //we run from column #0 till column #len-2, len-1 (which is the the last column) is for reference only
                mPressureRowData[col] = calculatePressure(rowCapacitanceData[col]);
            }
            for(WSICalculatedDataReceiver listener : listeners){
                listener.PressureDataReceived(mPressureRowData, rowNumber, len);
            }
        }

    }

    /**********************************************************************************************
     * Calculate the Pressure according to a given capacitance according to a given pixel coordinates
     * Return, The calculated Pressure as uint8
     **********************************************************************************************/
    byte calculatePressure(float capacitance)
    {
        long region = 0;
        boolean regionFound = false;
        while(!regionFound && region <  mpLut.regionsList.length) {
            if(capacitance < mpLut.regionsList[(int)region].upperBound && capacitance >= mpLut.regionsList[(int)region].lowerBound) {
                regionFound = true;
            }
            else {
                region++;
            }
        }
        float temp = 0.0f;
        if(region <  mpLut.regionsList.length) {
            for (byte j = 0; j < WSCommonDefs.kLutMaxCoefNum; ++j) {
                temp += Math.pow(capacitance, j) * mpLut.regionsList[(int) region].coefList[j];
                //Log.d("calculatePressure", "1- temp = " + temp + " , j = " + j + " , mpLut.regionsList[(int)region].coefList[j] = " + mpLut.regionsList[(int) region].coefList[j]);
            }
            if (temp < 0.0f) {
                temp = 0.0f;
            }
            if (temp > 255.0f) {
                temp = 255.0f;
            }
        }
        Float floatData = temp;
        byte res = floatData.byteValue();
        //Log.d("calculatePressure","2- temp = " + temp + " , res = " + res);
        return res;
    }

}
