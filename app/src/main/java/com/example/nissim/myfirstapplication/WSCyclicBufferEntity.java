package com.example.nissim.myfirstapplication;

import android.util.Log;

/**
 * Created by nissim on 5/18/2016.
 */
public class WSCyclicBufferEntity {
    public WSPsmData    psmData;
    public int          numOfConsumersRead;
    // Constructor
    public WSCyclicBufferEntity()
    {
        Log.d("WSCyclicBufferEntity","Create psmData");
        numOfConsumersRead = 0;
    }

    public void SetMatrixSize(int numOfColumns,int numOfRows)
    {
        psmData = new WSPsmData();
        psmData.SetMatrixDimension(numOfRows, numOfColumns);
    }
}
