package com.example.nissim.myfirstapplication;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nissim on 2/18/2016.
 */
public class WSPsmProxyImpl implements WSPsmProxy {
    private WSScuCommPkg scuCommPkg;
    private WSIRawDataReceiver rowDataConsumer;
    WSCommPkgDefs.HwcParams mpHwcParams;
    WSCommPkgDefs.OvcParams mpOvcParams;
    int                   mRowNumber;
    static  float         mCapacitance[];

    private List<WSIRawDataReceiver> listeners = new ArrayList<WSIRawDataReceiver>();


    public WSPsmProxyImpl(Context context) {
        scuCommPkg = WSScuCommPkg.getInstance(context);
        scuCommPkg.register(this);
    }

    public void RegisterRowDataConsumer(WSIRawDataReceiver callback)
    {
        listeners.add(callback);
    }
    public void StartOperation()
    {
        scuCommPkg.StartOperation();
    }

    public void Ready()
    {
        scuCommPkg.Ready();
    }

    public void RowDataReceived(short pVal[], byte pAmp[], int rowNumber, byte len)
    {
        Log.d("PsmProxyImple", "RowDataReceived for Row " + rowNumber);
        if(rowNumber == 0 || rowNumber == (mRowNumber+1)) {
            mRowNumber = rowNumber;
            for(int col=0;col<len-1;col++){ //we run from column #0 till column #len-2, len-1 (which is the the last column) is for reference only
                mCapacitance[col] = calculateCapacitance(pVal[col], pAmp[col],pVal[len-1],pAmp[len-1],mpHwcParams.cConst);
            }
        }
        for(WSIRawDataReceiver listener : listeners){
            listener.NewRawDataReceived(mCapacitance, rowNumber, len);
        }
    }

    public void LutDataReceived(WSCommPkgDefs.Lut lut,short numOfRegions)
    {
        for(WSIRawDataReceiver listener : listeners){
            listener.SetLutData(lut, numOfRegions);
        }
    }

    public void HwcParamsReceived(WSCommPkgDefs.HwcParams hwcParams)
    {
        mpHwcParams = new WSCommPkgDefs.HwcParams();
        mpHwcParams = hwcParams;
    }

    public void OvcParamsReceived(WSCommPkgDefs.OvcParams ovcParams)
    {
        mpOvcParams = new WSCommPkgDefs.OvcParams();
        mpOvcParams = ovcParams;
        mCapacitance = new float[mpOvcParams.overlaySizeCols];
        for(WSIRawDataReceiver listener : listeners){
            listener.OvcParamsReceived(ovcParams);
        }
    }

    public void PsmDisconnectedReceived()
    {
        for(WSIRawDataReceiver listener : listeners){
            listener.PsmDisconnectedReceived();
        }

        Log.d("PsmProxyImple", "PSM Disconnected");
    }

    public void PsmConnectedReceived()
    {
        for(WSIRawDataReceiver listener : listeners){
            listener.PsmConnectedReceived();
        }

        Log.d("PsmProxyImple", "PSM Connected");
    }

    /**********************************************************************************************
     * Calculate the Capcitance according to a given voltage , amplitude on the pixel and a refernece
     * Return, The calculated Capcitance
     **********************************************************************************************/
    public float calculateCapacitance(short val, byte amp,short refVal, byte refAmp, float cConst)
    {
        float capacitance = ((((float)val/(float)amp)) / ((float)refVal/(float)(refAmp))) * cConst;
        //Log.d("calculateCapacitance","val = " + val + " , amp = " + amp + "refVal = " + refVal + " , refAmp = " + refAmp + " , cConst = " + cConst + " , Capacitane = " + capacitance);
        return capacitance;
    }
}
