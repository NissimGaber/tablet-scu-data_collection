package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 2/18/2016.
 */
public interface WSPsmProxy {
    public void RowDataReceived(short pVal[], byte pAmp[], int rowNumber, byte len);
    public void LutDataReceived(WSCommPkgDefs.Lut lut,short numOfRegions);
    public void HwcParamsReceived(WSCommPkgDefs.HwcParams hwcParams);
    public void OvcParamsReceived(WSCommPkgDefs.OvcParams ovcParams);
    public void PsmDisconnectedReceived();
    public void PsmConnectedReceived();
}
