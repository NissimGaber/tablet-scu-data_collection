package com.example.nissim.myfirstapplication;

import android.util.Log;

/**
 * Created by nissim on 2/23/2016.
 */
public class WSCommonDefs {

    public class FirstStageBootLoaderVersion {
        short major;
        short minor;
        short build;
    }
    public class SecondStageBootLoaderVersion {
        short major;
        short minor;
        short build;
    }
    public class UserCodeVersion {
        short major;
        short minor;
        short build;
    }
    public class HwcVersion {
        FirstStageBootLoaderVersion firstStageVersion;
        SecondStageBootLoaderVersion secondStageVersion;
        UserCodeVersion userCode;
    }
    public class ScuVersion {
        short major;
        short minor;
        short build;
    }
    /**
     * the max number of coefficients in the LUT for each region
     */
    static final byte  kLutMaxCoefNum = 6;
    static final short kMaxNumLutRegions = 10;
    static final byte  kMaxOverlayCols = 40;
    static final byte  kMaxOverlayRows = 80;

    /**
     * Each channel is a temperature sensor
     */
    public enum TemperatureChannels {
        kLocal(0),
        kRemote1(1),
        kRemote2(2);
        int id;
        TemperatureChannels(int i) {
            id = i;
        }
    }

    public static void getEnum(int X) {
        TemperatureChannels temp = TemperatureChannels.values()[X];
        Log.d("checkEnumSize","ENUM =  " + temp);
    }
}


