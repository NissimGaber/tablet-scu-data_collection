package com.example.nissim.myfirstapplication;

import android.util.Log;

import java.util.concurrent.SynchronousQueue;

/**
 * Created by nissim on 5/18/2016.
 */
public class WSPsmDataConsumer  extends Thread implements WSIPsmDataConsumer {
    SynchronousQueue queue;
    public WSIPsmDataConsumer.WSDataCollectionMessage inMsg;
    private boolean  isLastDataConsumed;
    private WSDataReceivedHub dataReceiver;


    WSPsmDataConsumer() {

        queue = new SynchronousQueue<WSIPsmDataConsumer.WSDataCollectionMessage>();
    }

    public void AddDataReceiver(WSDataReceivedHub receiver)
    {
        dataReceiver = receiver;
    }

    public void    SetDataRead(boolean hasBeenRead){ isLastDataConsumed = hasBeenRead;}

    public boolean HasReadLastData() { return isLastDataConsumed;}

    public void NewMessageArrived(WSIPsmDataConsumer.WSDataCollectionMessage  dataCollectionMessage)
    {
        try {
            queue.put(dataCollectionMessage);
        } catch (InterruptedException e) {
            Log.d("WSPsmDataConsumer","Error Happened in put to the queue in thread " + Thread.currentThread().getName());
            e.printStackTrace();
        }
    }
    public void run() {
        String msg = new String();
        while (true) {
            try {
                inMsg = (WSIPsmDataConsumer.WSDataCollectionMessage)queue.take();
            } catch (InterruptedException e) {
                Log.d("WSDataReceivedHub", "Error Happened in take to the queue");
                e.printStackTrace();
            }
            if(inMsg.eMsg == WSIPsmDataConsumer.WSEDataCollectionMsg.kNewDataReceived) {
                WSPsmData inData;
                inData = dataReceiver.ReadPressureData(this,inMsg.parameter);
                dataReceiver.FinishedConsumingData(this,inMsg.parameter);
                msg = ("Consumer data for thread " + Thread.currentThread().getName()+": time : " + inData.SampleTimeStamp+"\n");
                for(int row=0 ;row<inData.mPressureVec.length;row++) {
                    msg += (" Row " + row + " : ");
                    for (int col = 1; col < inData.mPressureVec[row].length; col++) {
                        msg += inData.mPressureVec[row][col] + " , ";
                    }
                    msg += "\n";
                }
                System.out.println(msg);
            }
        }
    }
}
