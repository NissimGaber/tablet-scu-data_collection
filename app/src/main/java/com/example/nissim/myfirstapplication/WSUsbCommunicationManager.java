package com.example.nissim.myfirstapplication;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WSUsbCommunicationManager {
    static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static WSUsbCommunicationManager instance = null;

    UsbManager usbManager;
    UsbDevice usbDevice;
    UsbInterface intf = null;
    UsbEndpoint input, output;
    UsbDeviceConnection connection;

    PendingIntent permissionIntent;

    Context context;

    byte[] writeBytes = new byte[64];
    UsbSerialDevice serialPort;
    private List<WSScuCommPkgInterface> listeners = new ArrayList<WSScuCommPkgInterface>();

    protected  WSUsbCommunicationManager(Context context)
    {
        this.context = context;
        usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

        // ask permission from user to use the usb device
        permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        context.registerReceiver(usbReceiver, filter);
    }

    public static WSUsbCommunicationManager getInstance(Context context) {
        if(instance == null) {
            instance = new WSUsbCommunicationManager(context);
        }
        return instance;
    }

    public void register(WSScuCommPkgInterface callback) {
        listeners.add(callback);
    }

    public boolean connect()
    {
        String text = "ABCDEF";
        byte[] textByte = text.getBytes();
        Log.d("connect", "Text is: " + new String(textByte));
        // check if there's a connected usb device
        if(usbManager.getDeviceList().isEmpty())
        {
            Log.d("connect", "No connected devices");
            return false;
        }

        // get the first (only) connected device
        usbDevice = usbManager.getDeviceList().values().iterator().next();
        if(usbDevice == null){
            Log.d("connect", "usb device null , because getDeviceList return null");
            return false;
        }
        // user must approve of connection
        usbManager.requestPermission(usbDevice, permissionIntent);
        return true;
    }

    public void stop()
    {
        context.unregisterReceiver(usbReceiver);
    }

    public String send(byte writeData[])
    {
        Arrays.fill(writeBytes, (byte) 0);
        if(usbDevice == null)
        {
            return "no usb device selected";
        }

        int sentBytes = 0;
        if(writeData.length > 0)
        {
            synchronized(this)
            {
                // send data to usb device
                Log.d("send","send legth = " + writeData.length);
                Log.d("send","send Data = " + writeData);
                if (connection == null) {
                    Log.d("send", "connection is null");
                    return "Failed to open";
                } else {
                    //sentBytes = connection.bulkTransfer(output, writeBytes, writeBytes.length, 1000);
                    serialPort.write(writeData);
                }
            }
        }

        return Integer.toString(sentBytes);
    }
    private void setupConnection() {
        // find the right interface
        if (usbDevice == null) {
            Log.d("setupConnection", "USB device is NULL");
            return;
        } else {
            for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
                // communications device class (CDC) type device
                //if (usbDevice.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_CDC_DATA) {
                if ((usbDevice.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_VENDOR_SPEC) ||
                        (usbDevice.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_CDC_DATA)){
                    intf = usbDevice.getInterface(i);
                    if(intf == null) {
                        Log.d("setupConnection","intf is null for i = " + i);
                    }
                    else {
                        Log.d("setupConnection","intf is ok for i = " + i);
                    }
                    // find the endpoints
                    for (int j = 0; j < intf.getEndpointCount(); j++) {
                        if (intf.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_OUT && intf.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                            // from android to device
                            output = intf.getEndpoint(j);
                        }

                        if (intf.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_IN && intf.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                            // from device to android
                            input = intf.getEndpoint(j);
                        }
                    }
                }
                else {
                    Log.d("setupConnection","interface class = " + usbDevice.getInterface(i).getInterfaceClass());
                }
            }
        }
    }
    private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback()
    {
        @Override
        public void onReceivedData(byte[] arg0)
        {
            Log.d("OnReceivedData","In Data = " + new String(arg0));
            for(WSScuCommPkgInterface listener : listeners){
                listener.readMessageCallBack(arg0);
            }
        }
    };

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if(usbDevice == null) {
                Log.d("BroadcastReceiver","usbDevice is null");
            }
            else {
                if (ACTION_USB_PERMISSION.equals(action)) {
                    // broadcast is like an interrupt and works asynchronously with the class, it must be synced just in case
                    synchronized (this) {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            setupConnection();

                            connection = usbManager.openDevice(usbDevice);
                            if (connection == null) {
                                Log.d("BroadcastReceiver", "Failed to open USB device");
                                return;
                            } else {
                                if (intf == null) {
                                    Log.d("BroadcastReceiver", "interface is null");
                                    return;
                                } else {
                                    serialPort = UsbSerialDevice.createUsbSerialDevice(usbDevice, connection);
                                    if(serialPort != null)
                                    {
                                        if(serialPort.open()) {
                                            Log.d("BroadcastReceiver","Usb Opeened successfuly");
                                            // Devices are opened with default values, Usually 9600,8,1,None,OFF
                                            // CDC driver default values 115200,8,1,None,OFF
                                            serialPort.setBaudRate(38400);
                                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                                            serialPort.read(mCallback);
                                            Log.d("BroadcastReceiver", "usbDevice openned and assigend callback to the read");
                                        }else
                                        {
                                            // Serial port could not be opened, maybe an I/O error or it CDC driver was chosen it does not really fit
                                        }
                                    }else
                                    {
                                        // No driver for given device, even generic CDC driver could not be loaded
                                    }
                                }
                            }
                        } else {
                            Log.d("BroadcastReceiver", "Permission denied for USB device");
                        }
                    }
                } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    Log.d("BroadcastReceiver", "USB device detached");
                }
            }
        }
    };
}
