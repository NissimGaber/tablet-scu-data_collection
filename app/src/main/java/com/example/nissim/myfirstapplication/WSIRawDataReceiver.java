package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 4/26/2016.
 */
public interface WSIRawDataReceiver {
    public void PsmDisconnectedReceived();
    public void PsmConnectedReceived();
    public void OvcParamsReceived(WSCommPkgDefs.OvcParams ovcParams);
    public void SetLutData(WSCommPkgDefs.Lut lut,short numOfRegions);
    public void NewRawDataReceived(float rowCapacitanceData[], int rowNumber, byte len);
}
