package com.example.nissim.myfirstapplication;

/**
 * Created by nissim on 2/18/2016.
 */
public class WSPsmData {
    public static byte                  mPressureVec[][];
    public static float                 mCapacitanceVec[][];
    public static WSPatientToBodyPart   mBodyParts[];
    public static WSBodyPosition        mBodyPosition;
    public static long                  SampleTimeStamp=0;
    public static int                   mRowNumber=0;
    public final int                    mNumOfBudyParts=13;
    public void SetMatrixDimension(int rows,int columns)
    {
        mPressureVec    = new byte[rows][columns];
        mCapacitanceVec = new float[rows][columns];
        mBodyParts      = new WSPatientToBodyPart[mNumOfBudyParts];
        mBodyPosition   = new  WSBodyPosition();
    }
}
