package com.example.nissim.myfirstapplication;

import android.util.Log;

import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Description: This class responsible for the interface between the data collection
 *              and it's clients, it is responsible of controlling data in a  cyclic buffer, upon receiving
 *              new data the module writes a message identifying to it's all registered clients upon new data collected
 *              a new data shall be continue to be inserted in the cyclic buffer and in case the client don't clean the buffer
 *              an exception shall be fired when the buffer is full.
 *
 * Created by nissim on 5/9/2016.
 */
public class WSDataReceivedHub implements WSICalculatedDataReceiver, WSIRawDataReceiver{

    private class WSConsumerDataArrayElement {
        public WSIPsmDataConsumer   dataConsumer;
        public boolean              hadConsumed;
    }

    final   byte                                bufferSize=5;
    WSPsmDataCyclicBuffer                       mCyclicPsmData = new WSPsmDataCyclicBuffer(bufferSize);
    WSCyclicBufferEntity                        bufferEntity = new WSCyclicBufferEntity();;
    private List<WSConsumerDataArrayElement>    listeners = new ArrayList<WSConsumerDataArrayElement>();
    WSIPsmDataConsumer.WSDataCollectionMessage  dataCollectionMessage = new WSIPsmDataConsumer.WSDataCollectionMessage();
    boolean                                     isNextSampleStarted = false;
    int                                         numOfRowsInMatrix=0;
    int                                         numOfColumnsInMatrix=0;
    int                                         currentRow=0;

    //SynchronousQueue queue;

    /*********************************************************************************
     * This method allows consumer to be added to the listeners of the data collection
     * @param callback
     */
    public void AddListener(WSIPsmDataConsumer callback)
    {
        WSConsumerDataArrayElement dataConsumerElement = new WSConsumerDataArrayElement();
        dataConsumerElement.dataConsumer = callback;
        dataConsumerElement.hadConsumed = false;
        listeners.add(dataConsumerElement);
        callback.AddDataReceiver(this);
        mCyclicPsmData.AddNewConsumer();
        Log.d("WSDataReceivedHub", "AddListener, listener = " + callback);
    }

    /******************************************************************************
     * Recieved each time the PSM is connected or on startup, all the data lists is
     * constructed according to the number of columns in each row of the PSM
     * @param ovcParams
     */
    public void OvcParamsReceived(WSCommPkgDefs.OvcParams ovcParams)
    {
        numOfRowsInMatrix = ovcParams.overlaySizeRows;
        numOfColumnsInMatrix = ovcParams.overlaySizeCols;
        mCyclicPsmData.SetMatrixSize(ovcParams.overlaySizeCols, ovcParams.overlaySizeRows);
        bufferEntity.SetMatrixSize(ovcParams.overlaySizeCols, ovcParams.overlaySizeRows);
        Log.d("WSDataReceivedHub", "OvcParamsReceived, new Ovc Params received");
    }

    /********************************************************************************
     * This method is called when a raw data for the given row is received
     * @param capacitancePsmData
     * @param rowNumber
     * @param len
     */
    public void NewRawDataReceived(float capacitancePsmData[], int rowNumber, byte len)
    {
        for(int col=0;col<len-1;col++) {
            bufferEntity.psmData.mCapacitanceVec[rowNumber][col] = capacitancePsmData[col];
        }
        if(isNextSampleStarted == false) {
            Log.d("WSDataReceivedHub","Capacitance Received First");
            isNextSampleStarted = true;
        }
        else {
            InsertDataToContainer(rowNumber,len);
            isNextSampleStarted = false;
        }
        Log.d("WSDataReceivedHub", "NewRawDataReceived, new Capacitance data row received");
    }

    /***************************************************************
     * Activated when the lookup table is been read from the psm
     * @param lut
     * @param numOfRegions
     */
    public void SetLutData(WSCommPkgDefs.Lut lut,short numOfRegions)
    {
        Log.d("WSDataReceivedHub", "SetLutData , Need to store Calibration table in the data base");
    }


    /***************************************************************
     * Called when the pressure data is been calculated
     * @param calculatedDataRow
     * @param rowNumber
     * @param len
     */
    public void PressureDataReceived(byte calculatedDataRow[], int rowNumber, byte len)
    {
        for(int col=0;col<len-1;col++) {
            bufferEntity.psmData.mPressureVec[rowNumber][col] = calculatedDataRow[col];
        }
        bufferEntity.psmData.mRowNumber = rowNumber;
        if(isNextSampleStarted == false) {
            Log.d("WSDataReceivedHub","Pressure Received First");
            isNextSampleStarted = true;
        }
        else {
            InsertDataToContainer(rowNumber,len);
            isNextSampleStarted = false;
        }
        Log.d("WSDataReceivedHub","Pressure Data Received");
    }

    /**********************************************************************
     * This method responsible for filling the new data inside the cyclic buffer and sends a notification
     * to its data consumers including the position on the cyclic buffer
     * @param rowNumber
     * @param len
     */
    public void InsertDataToContainer(int rowNumber, byte len)
    {
        if(rowNumber == 0 || (rowNumber == currentRow + 1)) {
            currentRow = rowNumber;
            bufferEntity.psmData.SampleTimeStamp = System.currentTimeMillis();
            int index = mCyclicPsmData.add(rowNumber, bufferEntity);
            if((rowNumber + 1) == numOfRowsInMatrix) {
                for (WSConsumerDataArrayElement listener : listeners) {
                    dataCollectionMessage.eMsg = WSIPsmDataConsumer.WSEDataCollectionMsg.kNewDataReceived;
                    dataCollectionMessage.parameter = index;
                    listener.dataConsumer.NewMessageArrived(dataCollectionMessage);
                }
            }
        }
        else {
            Log.e("WSDataReveivedHub","Missed row reading , received "+rowNumber+" , expected "+currentRow+1);
        }
    }

    /******************************************************************************
     * This method reads the PsmData according to the index that had been sent on the notification message
     * @param consumer
     * @param index
     * @return
     */
    private WSCyclicBufferEntity ReadPsmData(WSIPsmDataConsumer consumer, int index)
    {
        Log.d("WSDataReceivedHub","read from index = " + index + " , for consumer " + consumer);
        WSCyclicBufferEntity psmData=null;
        boolean consumerFound = false;
        Iterator<WSConsumerDataArrayElement> itr = listeners.iterator();
        while (itr.hasNext()) {
            if(itr.next().dataConsumer == consumer) {
                psmData = mCyclicPsmData.getByIndex(index);
                consumerFound = true;
                break;
            }
        }
        if(!consumerFound) {
            Log.e("WSDataReceivedHub","consumer " + consumer + " is not a registered listener");
            throw new BufferUnderflowException();
        }
        return psmData;
    }

    public float[][] ReadRawData(WSIPsmDataConsumer consumer, int index)
    {
        WSCyclicBufferEntity readData=null;
        readData = ReadPsmData(consumer,index);
        return readData.psmData.mCapacitanceVec;
    }

    public WSPsmData ReadPressureData(WSIPsmDataConsumer consumer, int index)
    {
        WSCyclicBufferEntity bufferEntity=null;
        bufferEntity = ReadPsmData(consumer,index);
        return bufferEntity.psmData;
    }

    public void FinishedConsumingData(WSIPsmDataConsumer consumer, int index)
    {
        Log.d("WSDataReceivedHub","read from index = " + index + " , for consumer " + consumer);
        WSCyclicBufferEntity psmData=null;
        boolean consumerFound = false;
        Iterator<WSConsumerDataArrayElement> itr = listeners.iterator();
        while (itr.hasNext()) {
            if(itr.next().dataConsumer == consumer) {
                boolean released = mCyclicPsmData.FinishDataConsuming(index);
                Log.d("WSDataReceivedHub","Released the Cell");
                consumerFound = true;
                break;
            }
        }
        if(!consumerFound) {
            Log.e("WSDataReceivedHub", "consumer " + consumer + " is not a registered listener");
            throw new BufferUnderflowException();
        }
    }

    public void PsmDisconnectedReceived()
    {
        Log.d("WSDataReceivedHub", "PSM Disconnected");
    }

    public void PsmConnectedReceived()
    {
        Log.d("WSDataReceivedHub", "PSM Connected");
    }

}
